Installation Instructions:
Run setup file by entering "bash setup.sh" in the terminal

Compatability:
Most linux distributions (tested using ubuntu 18.10)

Usage:
1. Write your code in a file with an extension .japl in the root directory of the compiler.
2. Type "cabal run <filename>" in terminal replacing <filename> with the name of your file without the .japl extension.
3. Run your program using the file name of your code file without the .japl extension. 

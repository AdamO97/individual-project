module CodeGenerator
(
  run,

) where

--import ExpressionTree as ET
import SyntaxTree as ST

-- test = While (ET.GT (Leaf 4) (Leaf 5)) [(Print (Sum (Div (Leaf 8) (Leaf 4)) (Leaf (5))))]
-- test2 = Print (Leaf 4)
-- test3 = Print (Product (Leaf 4) (Leaf (5)))
-- test4 = If (ET.GT (Leaf 3) (Leaf 4)) [(Print (Leaf 4))] [(Print (Leaf 5))]


evalAOP (ABinary Sum a b) labelList = (evalAExpr a labelList "rax") ++ (evalAExpr b labelList "rbx") ++ "add rax, rbx" ++ nl
evalAOP (ABinary Product a b) labelList = (evalAExpr a labelList "rax") ++ (evalAExpr b labelList "rbx") ++ "imul rax, rbx" ++ nl
evalAOP (ABinary Div a b) labelList = (evalAExpr a labelList "rax") ++ (evalAExpr b labelList "rbx") ++ "idiv rbx" ++ nl
evalAOP (ABinary Sub a b) labelList = (evalAExpr a labelList "rax") ++ (evalAExpr b labelList "rbx") ++ "sub rax, rbx" ++ nl


evalAExpr a labelList reg
  | (isVar a) = "mov " ++ reg ++ ", [" ++ (iden a) ++ "]"++nl
  | (isIConst a) = "mov " ++ reg ++ ", " ++ (show (iConst a)) ++ nl
  | (isABinary a) = evalAOP a labelList ++ if (reg == "rax") then "" else ("mov " ++ reg ++ ", rax\n")

isIConst (IntConst _) = True
isIConst _ = False

isVar (Var _) = True
isVar _ = False

isABinary (ABinary _ _ _) = True
isABinary _ = False

evalStatement :: Stmt -> [String] -> (String, [String])
evalStatement (If cond c d) labelList = let x=(createLabel "if" labelList 0)
                                            y=(createLabel "end" labelList 0)
                                            seqd = (evalSeqStatement d ("", (x:y:labelList)))
                                            seqc = (evalSeqStatement c ("", (snd seqd)))
                                        in (((processCondition cond x) ++ (fst seqd) ++ "jmp " ++ y ++ "\n" ++ x ++ ":\n" ++ (fst seqc) ++ y ++ ":\n"), (snd seqc))
evalStatement (While cond c) labelList = let x=(createLabel "while" labelList 0)
                                             code=(createLabel "code" labelList 0)
                                             y=(createLabel "end" labelList 0)
                                             seqCode = (evalSeqStatement c ("", (x:code:y:labelList)))
                                         in ((x ++ ":\n" ++ (processCondition cond code) ++ "jmp " ++ y ++ nl ++ code ++ ":" ++ nl ++ (fst seqCode) ++ "jmp " ++ x ++ nl ++ y ++ ":\n"), (snd seqCode))
evalStatement (For a cond b c) labelList = let x=(createLabel "for" labelList 0)
                                               code=(createLabel "code" labelList 0)
                                               y=(createLabel "end" labelList 0)
                                               seqCode = (evalSeqStatement c ("", (x:code:y:labelList)))
                                             in (((fst (evalStatement a labelList)) ++ x ++ ":\n" ++ (processCondition cond code) ++ "jmp " ++ y ++ nl ++ code ++ ":" ++ nl ++ (fst seqCode) ++ (fst (evalStatement b labelList))++ "jmp " ++ x ++ nl ++ y ++ ":\n"), (snd seqCode))
evalStatement (Assign a b) labelList = (((evalAExpr b labelList "rax") ++ "mov [" ++ a ++ "], rax" ++ nl), labelList)
evalStatement (Print a) labelList = (((evalAExpr a labelList "rax") ++ "call _printRAX" ++ nl), labelList)
evalStatement (End) labelList = ("", labelList)

evalSeqStatement :: SeqStmt -> (String, [String]) -> (String, [String])
evalSeqStatement (Seq []) x = x
evalSeqStatement a (code, labelList) = let x = head (sequ a)
                                           xs = Seq (tail (sequ a))
                                           statement = evalStatement x labelList
                                       in  (evalSeqStatement xs (code ++ (fst statement), (snd statement)))


  -- | ((sequ a) == []) = ""
  -- | otherwise = let x = head (sequ a)
  --                   xs = Seq (tail (sequ a))
  --                   statement = evalStatement x labelList
  --               in (fst statement) ++ (evalSeqStatement xs (snd statement))

--convert :: ET.ExpressionTree -> [String] -> [String] -> String
-- convert (If cond c d) varList labelList = let x=(createLabel "if" labelList 0)
--                                               y=(createLabel "end" labelList 0)
--                                           in (processCondition cond) ++ x ++ "\n" ++ (compile d) ++ "jmp " ++ y ++ "\n" ++ x ++ ":\n" ++ (compile c) ++ y ++ ":\n"


--convert (Product (Leaf a) (Leaf b)) varList labelList = "mov rax, "++(show a)++"\nimul rax, "++(show b)++"\n"
--convert (Sum (Leaf a) (Leaf b)) varList labelList= "mov rax, "++(show a)++"\nadd rax, "++(show b)++"\n"
--convert (Sum a b) varList labelList = (convert a varList labelList) ++ (convert b varList labelList) ++ "pop rax" ++ nl ++ "pop rbx" ++ nl ++ "add rax, rbx" ++ nl ++ "push rax" ++ nl
--convert (Product a b) varList labelList = (convert a varList labelList) ++ (convert b varList labelList) ++ "pop rax" ++ nl ++ "pop rbx" ++ nl ++ "imul rax, rbx" ++ nl ++ "push rax" ++ nl
--convert (Div a b) varList labelList = (convert a varList labelList) ++ (convert b varList labelList) ++ "pop rbx" ++ nl ++ "pop rax" ++ nl ++ "idiv rbx" ++ nl ++ "push rax" ++ nl
--convert (Print (VarCall a)) varList labelList = "mov rax, "++"&" ++ a ++ "\ncall _printRAX\n"
--convert (Print (Leaf a)) varList labelList = "mov rax, "++(show a)++"\ncall _printRAX\n"
--convert (Print a) varList labelList = (convert a varList labelList) ++ "pop rax" ++ nl ++ "call _printRAX" ++ nl
--convert (VarAss a b) varList labelList =  (convert b varList labelList) ++ "pop rax"++nl++"mov [" ++ a ++ "], rax"++nl
--convert (VarCall a) varList labelList = "mov rax, [" ++ a ++ "]"++nl
--convert (While cond c) varList labelList = let x = createLabel "while" labelList 0
                                           --in x ++ ":\n" ++ (compile c) ++ (processCondition cond) ++ x ++ "\n"
--convert (Leaf a) varList labelList = "push " ++ (show a) ++ nl
--convert _ _ _ = "error"


dataelement a = a ++ " resw 8" ++nl



-- isLeaf :: ET.ExpressionTree -> Bool
-- isLeaf (Leaf _) = Prelude.True
-- isLeaf _ = Prelude.False
--
-- getLeaf (Leaf a) = a

--Takes a condition and returns assembly code that makes a comparison and checks the Condition
--processCondition :: Condition -> String

processCondition (RBinary op a b) jmpPointT
  | (isGT op) = (cmp a b) ++ "jg " ++ jmpPointT ++ nl
  | (isLT op) = (cmp a b) ++ "jl " ++ jmpPointT ++ nl
  | (isGTE op) = (cmp a b) ++ "jge " ++ jmpPointT ++ nl
  | (isLTE op) = (cmp a b) ++ "jle " ++ jmpPointT ++ nl
  | (isEQ op) = (cmp a b) ++ "je " ++ jmpPointT ++ nl
processCondition (BoolConst True) jmpPointT = "jmp " ++ jmpPointT ++ nl
processCondition _ _ = ""

isGT ST.GT = True
isGT _ = False

isLT ST.LT = True
isLT _ = False

isGTE GTE = True
isGTE _ = False

isLTE LTE = True
isLTE _ = False

isEQ ST.EQ = True
isEQ _ = False

-- processCondition (ET.GT a b) = (cmp a b) ++ "jg "
-- processCondition (ET.LT a b) = (cmp a b) ++ "jl "
-- processCondition (ET.GTE a b) = (cmp a b) ++ "jge "
-- processCondition (ET.LTE a b) = (cmp a b) ++ "jle "
-- processCondition (ET.EQ a b) = (cmp a b) ++ "je "
-- processCondition (ET.True) = "jmp "
-- processCondition _ = ""






cmp a b = (evalAExpr a [] "rax")++ (evalAExpr b [] "rbx") ++ "cmp rax, rbx" ++ nl



nl = "\n"
movRAX a = "mov rax, " ++ (show a) ++ "\n"
programStop = "mov rax, 60\nmov rdx, 0\nsyscall\n"
generate a = "section .text\n%include \"printing.asm\"\nglobal _start\n_start:\n" ++ (fst (evalSeqStatement a ("", []))) ++ programStop ++"\n"
dataSection a = "section .bss\n" ++ (fst (getVars (sequ a) ("", [])))




createLabel :: String -> [String] -> Int -> String
createLabel x y 0 = if (not (x `elem` y)) then x else createLabel x y 1
createLabel x y z = if (not (x++show(z) `elem` y)) then x++show(z) else createLabel x y (z+1)

-- compile [] = []
-- compile (x:xs) = (convert x [] []) ++ (compile xs)
--
-- isVarAss (VarAss _ _) = Prelude.True
-- isVarAss _ = Prelude.False
--
-- getVarName (VarAss a _) = a
-- getVarName _ = "Not a Var"
--
-- isIf (If _ _ _) = Prelude.True
-- isIf _ = Prelude.False
--
-- getIfCode (If _ a b) = (a, b)
--
-- isWhile (While _ _) = Prelude.True
-- isWhile _ = Prelude.False
--
-- getWhileCode (While _ a) = a

--getVars :: [ExpressionTree] -> [String] -> String
-- getVars [] _ = ""
-- getVars (x:xs) a
--     | (isVarAss x) = let var = getVarName x in (if (not (var `elem` a)) then ((dataelement var)++(getVars xs ([var]++a))) else (getVars xs a))
--     | (isIf x) = let code = getIfCode x
--                      block1 = (fst code)
--                      block2 = (snd code)
--                  in (getVars block1 a) ++ (getVars block2 a) ++ (getVars xs a)
--     | (isWhile x) = (getVars (getWhileCode x) a) ++ (getVars xs a)
--     | otherwise = getVars xs a


getStateVars (Assign a _) varList = ((if (not (a `elem` varList)) then (dataelement a) else ""), varList ++ [a])
getStateVars (For a _ b c) varList = let var1 = (fst (getStateVars a varList))
                                         varListNew = (snd (getStateVars a varList))
                                         var2 = (fst (getStateVars b varListNew))
                                         varListNew2 = (snd (getStateVars a varListNew))
                                         vars = (fst (getVars (sequ c) ("", varListNew2)))
                                         varsListCom = (snd (getVars (sequ c) ("", varListNew2)))
                                     in ((var1 ++ var2 ++ vars), varsListCom)
getStateVars (If _ a b) varList = let vars1 = (fst (getVars (sequ a) ("", varList)))
                                      varListNew = (snd (getVars (sequ a) ("", varList)))
                                      vars2 = (fst (getVars (sequ b) ("", varListNew)))
                                      varsListCom = (snd (getVars (sequ b) ("", varListNew)))
                                  in ((vars1 ++ vars2), varsListCom)
getStateVars (While _ a) varList = let vars = (fst (getVars (sequ a) ("", varList)))
                                       varsListCom = (snd (getVars (sequ a) ("", varList)))
                                   in (vars, varsListCom)
getStateVars _ varList = ("", varList)

getVars [] (vars, varList) = (vars, varList)
getVars (x:xs) (vars, varList) = let returned = (getStateVars x varList)
                                 in (getVars xs ((vars ++ fst returned), (snd returned)))


run a file = let fileString = (dataSection a)++(generate a)
        in do writeFile (file ++ ".asm") fileString

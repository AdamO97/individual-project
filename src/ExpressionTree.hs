module ExpressionTree
( ExpressionTree(..),
  Condition(..),
  Program(..)
) where
data Program = Program {
                    code :: [ExpressionTree]
}

data ExpressionTree = Empty | Leaf Int | VarAss String ExpressionTree | VarCall String | Sum ExpressionTree ExpressionTree | Sub ExpressionTree ExpressionTree | Product ExpressionTree ExpressionTree | Div ExpressionTree ExpressionTree | Print ExpressionTree | If Condition [ExpressionTree] [ExpressionTree] | While Condition [ExpressionTree]
data Condition = False | True | GT ExpressionTree ExpressionTree | LT ExpressionTree ExpressionTree | GTE ExpressionTree ExpressionTree | LTE ExpressionTree ExpressionTree | EQ ExpressionTree ExpressionTree

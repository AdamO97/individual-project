module Main where

import CodeGenerator as CG
import Parser as Parser
import System.Process
--import ExpressionTree
import SyntaxTree as ST
import System.Environment

main = do
   d <- getArgs
   p <- openAndParse ((d!!0) ++ ".japl")
   putStrLn (show p)
   run ((code p)) (d!!0)
   readProcess "nasm" ["-f", "elf64", "-o", ((d!!0) ++ ".o"), ((d!!0) ++ ".asm")] ""
   readProcess "ld" [((d!!0) ++ ".o"), "-o", (d!!0)] ""

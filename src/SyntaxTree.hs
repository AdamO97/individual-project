module SyntaxTree
( Program(..),
  AOP(..),
  AExpr(..),
  BExpr(..),
  BBinOp(..),
  RBinOp(..),
  Stmt(..),
  SeqStmt(..),
  Program(..)
) where


data AOP  = Sum
            | Sub
            | Product
            | Div
              deriving (Show)

data AExpr = Var {
                  iden :: String
                }
           | IntConst {
                        iConst ::Integer
                      }
           | Neg AExpr
           | ABinary AOP AExpr AExpr
            deriving (Show)

data BBinOp = And
            | Or
              deriving (Show)

data RBinOp = GT
            | LT
            | GTE
            | LTE
            | EQ
              deriving (Show)

data BExpr = BoolConst Bool
           | Not BExpr
           | BBinary BBinOp BExpr BExpr
           | RBinary RBinOp AExpr AExpr
            deriving (Show)



data Stmt = Assign String AExpr
          | If BExpr SeqStmt SeqStmt
          | While BExpr SeqStmt
          | For Stmt BExpr Stmt SeqStmt
          | Print AExpr
          | End
            deriving (Show)

data SeqStmt = Seq {
                  sequ :: [Stmt]
}deriving (Show)

data Program = Program {
                    code :: SeqStmt
}deriving (Show)

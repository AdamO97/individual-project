module Parser
( printMatch,
  openAndParse,
)where

import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token
import Text.Parsec
import Text.Parsec.Prim
import Text.Parsec.Combinator
import SyntaxTree as ST
import Data.List


printMatch = do
  reserved "print"
  spaces
  n <- aExpression
  return (Print (n))

varAss = do
  name <- identifier
  spaces
  reservedOp "="
  spaces
  tree <- aExpression
  return (Assign name tree)


ifStatement = do
  reserved "if"
  char '('
  cond <- bExpression
  char ')'
  char '{'
  whiteSpace
  p <- sequenceOfStmt
  char '}'
  whiteSpace
  p2 <- optionMaybe elseParser
  if (isNothing p2) then return (If cond p (Seq [])) else return (If cond p (getMaybe p2))

whileStatement = do
  reserved "while"
  char '('
  cond <- bExpression
  char ')'
  char '{'
  whiteSpace
  p <- sequenceOfStmt
  char '}'
  return (While cond p)

forStatement = do
  reserved "for"
  char '('
  assn1 <- varAss
  semi
  cond <- bExpression
  semi
  assn2 <- varAss
  char ')'
  char '{'
  whiteSpace
  p <- sequenceOfStmt
  char '}'
  return (For assn1 cond assn2 p)

getMaybe (Just a) = a

isNothing Nothing = True
isNothing _ = False

elseParser = do
  reserved "else"
  char '{'
  whiteSpace
  p <- sequenceOfStmt
  char '}'
  return p

sequenceOfStmt = do
    list <- (sepBy1 newParser semi)
    return (Seq list)

assumeRight (Right vv) = vv
assumeRight (Left vv) = error (show (vv))

newParser = printMatch
        <|> varAss
        <|> ifStatement
        <|> whileStatement
        <|> forStatement
        <|> eofParse

eofParse = do
  whiteSpace
  return End

openAndParse filePath = do
  content <- readFile filePath
  return (Program (assumeRight (parse sequenceOfStmt " " content)))


languageDef =
     emptyDef { Token.commentStart    = "/*"
              , Token.commentEnd      = "*/"
              , Token.commentLine     = "//"
              , Token.identStart      = letter
              , Token.identLetter     = alphaNum
              , Token.reservedNames   = [ "if"
                                        , "then"
                                        , "else"
                                        , "true"
                                        , "false"
                                        , "not"
                                        , "and"
                                        , "or"
                                        , "while"
                                        , "for"
                                        , "print"
                                        ]
              , Token.reservedOpNames = ["+", "-", "*", "/", "="
                                        , "<", ">", "and", "or", "not"
                                        ]
              }

lexer = Token.makeTokenParser languageDef

identifier = Token.identifier lexer -- parses an identifier
reserved   = Token.reserved   lexer -- parses a reserved name
reservedOp = Token.reservedOp lexer -- parses an operator
parens     = Token.parens     lexer -- parses surrounding parenthesis:
                                    --   parens p
                                     -- takes care of the parenthesis and
                                     -- uses p to parse what's inside them
integer    = Token.integer    lexer -- parses an integer
semi       = Token.semi       lexer -- parses a semicolon
whiteSpace = Token.whiteSpace lexer -- parses whitespace

aExpression :: Parser AExpr
aExpression = buildExpressionParser aOperators aTerm

bExpression :: Parser BExpr
bExpression = buildExpressionParser bOperators bTerm

aOperators = [ [Prefix (reservedOp "-"   >> return (Neg             ))          ]
             , [Infix  (reservedOp "*"   >> return (ABinary Product)) AssocLeft,
                Infix  (reservedOp "/"   >> return (ABinary Div )) AssocLeft]
             , [Infix  (reservedOp "+"   >> return (ABinary Sum     )) AssocLeft,
                Infix  (reservedOp "-"   >> return (ABinary Sub     )) AssocLeft]
              ]
bOperators = [ [Prefix (reservedOp "not" >> return (Not             ))          ]
             , [Infix  (reservedOp "and" >> return (BBinary And     )) AssocLeft,
                Infix  (reservedOp "or"  >> return (BBinary Or      )) AssocLeft]
             ]

aTerm =  parens aExpression
      <|> liftM Var identifier
      <|> liftM IntConst integer

bTerm =  parens bExpression
      <|> (reserved "true"  >> return (BoolConst True ))
      <|> (reserved "false" >> return (BoolConst False))
      <|> rExpression

rExpression =
   do a1 <- aExpression
      op <- relation
      a2 <- aExpression
      return $ RBinary op a1 a2

relation =   (reservedOp ">" >> return ST.GT)
         <|> (reservedOp "<" >> return ST.LT)
         <|> (reservedOp "<=" >> return ST.LTE)
         <|> (reservedOp ">=" >> return ST.GTE)
         <|> (reservedOp "==" >> return ST.EQ)

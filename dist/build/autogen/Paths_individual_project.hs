{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_individual_project (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/adam/.cabal/bin"
libdir     = "/home/adam/.cabal/lib/x86_64-linux-ghc-8.0.2/individual-project-0.1.0.0-ICtluBqJCW5EWOqkwQGkAN"
dynlibdir  = "/home/adam/.cabal/lib/x86_64-linux-ghc-8.0.2"
datadir    = "/home/adam/.cabal/share/x86_64-linux-ghc-8.0.2/individual-project-0.1.0.0"
libexecdir = "/home/adam/.cabal/libexec"
sysconfdir = "/home/adam/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "individual_project_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "individual_project_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "individual_project_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "individual_project_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "individual_project_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "individual_project_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)

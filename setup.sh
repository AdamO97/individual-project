#! /bin/sh
echo "This script will install all pre-requisites for the japl compiler. You may be required to enter your password at some stages so you can apt install packages."

echo "Installing GHC..."
sudo apt install -y ghc
echo "Installing Cabal..."
sudo apt install -y cabal-install
echo "Installing NASM..."
sudo apt install -y nasm
echo "Installing dependencies..."
cabal install --upgrade-dependencies
echo "Some errors may occur while installing dependencies however this should not be a concern as dependencies should still have installed correctly"
